# Community Fridges

- [Maria Arutyunova](https://gitlab.com/mariaarutyunova)
- [Carmen Lemoine](https://gitlab.com/carmen.lemoine)
- [Preeti Mahar](https://gitlab.com/preetimahar77)
- [Jahaziel Martinez](https://gitlab.com/jahmtzc)
- [Sam Siskind](https://gitlab.com/swamswiskind)

## Intended market

The web application within this project was built for visitors to community fridges and pantries as well as the decentralized volunteer networks that manage those community fridges and pantries. The hope is that the continued development of this project can reduce the operational effort required to maintain these networks so that more of their focus can be on fighting food insecurity.

## Functionality

### Visitors (Logged Out Users)

- Can signup for a Volunteer account.
- Can login to that account.
- Can view a list of all neighborhoods and their respective postal codes.
- Can view a list of all active fridges.
- Can search the list of all active fridges by fridge name, neighborhood name and address.
- Can see the details for a fridge.
- Can see the last check-in for a fridge.
- Can view a list of all pantries.
- Can see details for a pantry.
- Can see the last check-in for a pantry.
- Can check-in at a fridge.
- Can check-in at a pantry.
- Can request items for a fridge.
- Can request items for a pantry.

### Volunteers (Logged In Users)

- Can do everything a visitor can do, plus:
  - Can create new Neighborhoods.
  - Can create new Fridges.
  - Can create new Pantries.
  - Can view inactive Fridges.
  - Can view all Fridge Check-ins.
  - Can view all Pantry Check-ins.
  - Can view all Fridge Requests.
  - Can view all Pantry Requests.
  - Can view a list of all Volunteers.

## Running the project locally

1. Fork this project.

2. Clone the repository to your local machine.

3. CD into the new project directory.

4. Install [Docker Desktop](https://www.docker.com/products/docker-desktop/).

5. Start Docker Desktop.

6. In terminal, run these commands:

```
docker volume create postgres-data

docker volume create pg-admin

docker compose build

docker compose up
```

7. Visit [http://localhost:3000](http://localhost:3000) in your browser. When visiting the deployed site, you may need to enable third-party cookies before logging in or signing up.

Enjoy the Community Food website to its fullest!

## Deliverables

- [Wire-frame Diagrams](docs/wireframes.md)

- [API documentation](docs/API_Documentation.md)

- [Deployed Project](https://community-fridges-carmen-lemoine-902e201b2a003f7d9e63ff53668b47.gitlab.io/)

- [OpenAPI Swagger for this project](https://community-fridges.onrender.com/docs#)

- [Original Gitlab Issue Board](https://gitlab.com/fridgeraiders/community-fridges/-/boards/5807966)

- Team Journals:
  - [Maria's Journal](journals/maria_arutyunova.md)
  - [Carmen's Journal](journals/carmen_lemoine.md)
  - [Jahaziel's Journal](journals/jahaziel_martinez.md)
  - [Preeti's Journal](journals/preeti_mahar.md)
  - [Sam's Journal](journals/sam_siskind.md)
