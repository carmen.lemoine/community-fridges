# Community Fridges API Docs

To interact with our docs page locally please visit http://localhost:8000/docs. To visit our deployed documentation please visit https://mar-6-pt-community-fridges.mod3projects.com/docs#/

## To Interact With The Users End-Points

The Users endpoints are for CRUD operations on User accounts. Create is used for user account signup (and includes login authentication), while the GET endpoints are used for logged in views on the community fridges frontend.

```SQL
 [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            user_id serial not null primary key,
            first_name varchar(100) not null,
            pronouns varchar(150),
            email varchar(50) not null unique,
            hashed_password varchar(200) not null,
            type INT not null
            );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """,
    ]
```

### Get All Users (Protected Endpoint)

**Request Method:** GET

**Path:** /users/

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
[
  {
    "user_id": 0,
    "first_name": "string",
    "pronouns": "string",
    "email": "string",
    "type": 0
  },
  {
    "user_id": 0,
    "first_name": "string",
    "pronouns": "string",
    "email": "string",
    "type": 0
  }
]
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to get all users."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Get One User (Protected Endpoint)

**Request Method:** GET

**Path:** /users/{user_id}/

**Path Parameters:** user_id: int

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "user_id": 0,
  "first_name": "string",
  "pronouns": "string",
  "email": "string",
  "type": 0
}
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to get a user."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Create a User

**Request Method:** POST

**Path:** /api/users/

#### Request Body:

```json
{
  "first_name": "string",
  "pronouns": "string",
  "email": "string",
  "password": "string",
  "type": 0
}
```

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "user_id": 0,
    "first_name": "string",
    "pronouns": "string",
    "email": "string",
    "type": 0
  }
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Update One User (Protected Endpoint)

**Request Method:** PUT

**Path:** /users/{user_id}/

**Path Parameters:** user_id: int

#### Request Body:

```json
{
  "first_name": "string",
  "pronouns": "string",
  "email": "string",
  "type": 0
}
```

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "user_id": 0,
  "first_name": "string",
  "pronouns": "string",
  "email": "string",
  "type": 0
}
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to update a user."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Delete One User (Protected Endpoint)

**Request Method:** DELETE

**Path:** /users/{user_id}/

**Path Parameters:** user_id: int

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
true
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to delete a user."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

## To Interact With The Pantry End-Points

The Pantry endpoints are for CRUD operation on Pantries.

- SQL Table

```SQL
    [
        # "UP" SQL statment
        """
        CREATE TABLE pantries (
            id SERIAL PRIMARY KEY not NULL,
            active boolean not null,
            pantry_name varchar(200) not NULL,
            pantry_picture TEXT,
            neighborhood INT REFERENCES neighborhoods (id),
            location VARCHAR(250) not NULL,
            hours_start TIME not NULL,
            hours_end TIME not NULL,
            date_created date not null DEFAULT CURRENT_DATE

        );
        INSERT INTO pantries
            ( active, pantry_name, pantry_picture, neighborhood, location, hours_start, hours_end, date_created)
            VALUES
            (FALSE, 'Eagle Rock Community Pantry', 'https://cdn.pixabay.com/photo/2014/08/12/12/52/pantry-416596_1280.jpg', 1, '5032 Maywood Ave, Los Angeles, CA 90041', '08:00:00', '16:00:00', '2023-07-20'),
            (TRUE, 'Los Feliz Community Pantry', 'https://cdn.pixabay.com/photo/2021/02/20/15/38/pantry-6033796_1280.jpg', 2, '1733 N New Hampshire Ave Los Angeles, CA 90027', '00:00:00', '23:59:00', '2023-07-20'),
            (TRUE, 'Silver Lake Community Pantry', 'https://i.insider.com/5e6e4d27235c1814256da892?width=700', 3, '1515 Griffith Park Blvd Los Angeles, CA 90026', '07:30:00', '19:30:00', '2023-07-20'),
            (TRUE, 'El Sereno Community Pantry', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBF-03_ns5BqqFj8UXIAp9b373xcPS55gV2w&usqp=CAU', 4, '5469 Huntington Dr N Los Angeles, CA 90032', '00:00:00', '23:59:00', '2023-07-20');

        """,
        # "Down" SQL statement
        """
        DROP TABLE pantries;
        """,
    ],



```

### To Get All Pantries:

**Request Method:** GET

**Path:** /pantries/

Request:

```
There is no input, you would just click execute and receieve the below response.
```

Response:

```json
[
  {
    "id": 0,
    "active": true,
    "pantry_name": "string",
    "neighborhood": {
      "id": 0,
      "neighborhood_name": "string"
    },
    "pantry_picture": "string",
    "location": "string",
    "hours_start": "string",
    "hours_end": "string",
    "date_created": "2023-07-26"
  }
]
```

### Create a Pantry

**Request Method:** POST

**Path:** /pantries/{pantry_id}/

Request:

```json
{
  "active": true,
  "pantry_name": "string",
  "pantry_picture": "string",
  "neighborhood": 1,
  "location": "string",
  "hours_start": "string",
  "hours_end": "string",
  "date_created": "2023-07-26"
}
```

Response:

```json
{
  "id": 5,
  "active": true,
  "pantry_name": "string",
  "neighborhood": {
    "id": 1,
    "neighborhood_name": "Eagle Rock"
  },
  "pantry_picture": "string",
  "location": "string",
  "hours_start": "08:00:00",
  "hours_end": "10:00:00",
  "date_created": "2023-07-26"
}
```

### To Get One Single Pantry:

**Request Method:** GET

**Path:** /pantries/{pantry_id}/

**Path Parameters:** pantry_id: int

Request:

```
Enter the Pantry ID Number
```

Response:

```json
{
  "id": 1,
  "active": false,
  "pantry_name": "Eagle Rock Community Pantry",
  "neighborhood": {
    "id": 1,
    "neighborhood_name": "Eagle Rock"
  },
  "pantry_picture": "https://cdn.pixabay.com/photo/2014/08/12/12/52/pantry-416596_1280.jpg",
  "location": "5032 Maywood Ave, Los Angeles, CA 90041",
  "hours_start": "08:00:00",
  "hours_end": "16:00:00",
  "date_created": "2023-07-20"
}
```

### To Update A Pantry:

**Request Method:** PUT

**Path:** /pantries/{pantry_id}/

**Path Parameters:** pantry_id: int

Request:

Make sure to enter the ID of the pantry you
are trying to update. And then enter:

```json
{
  "active": true,
  "pantry_name": "string",
  "pantry_picture": "string",
  "neighborhood": 1,
  "location": "string",
  "hours_start": "08:00",
  "hours_end": "10:00",
  "date_created": "2023-07-26"
}
```

Response:

```json
{
  "id": 1,
  "active": true,
  "pantry_name": "string",
  "neighborhood": {
    "id": 1,
    "neighborhood_name": "Eagle Rock"
  },
  "pantry_picture": "string",
  "location": "string",
  "hours_start": "08:00:00",
  "hours_end": "10:00:00",
  "date_created": "2023-07-26"
}
```

### To Delete A Pantry:

**Request Method:** DELETE

**Path:** /pantries/{pantry_id}/

**Path Parameters:** pantry_id: int

Request:

```
Make sure to enter the ID of the pantry you
are trying to delete. Then hit execute.


```

Response:

```json
true
```

## To Interact With The Pantry Requests End-Points

This is so that anyone can make a Pantry Item Request.

- SQL Table

```SQL
 [
        # "Up" SQL statement
    [
        """
        CREATE TABLE pantry_requests(
            id serial not null primary key,
            pantry int REFERENCES pantries (id),
            date_created date not null DEFAULT CURRENT_DATE,
            additional varchar(500),
            visitor varchar(100)
        );
        """,
        # "DOWN" SQL statement
        """
        DROP TABLE pantry_requests;
        """,
    ],
    [
         # "Up" SQL statement
        """
        CREATE TABLE pantry_request_items (
        pantry_request_id INT NOT NULL,
        item_id INT NOT NULL,
        FOREIGN KEY (pantry_request_id) REFERENCES pantry_requests (id),
        FOREIGN KEY (item_id) REFERENCES items (id),
        PRIMARY KEY (pantry_request_id, item_id)
        );
        """,
        # "DOWN" SQL statement
        """
        DROP TABLE pantry_request_items;
        """,
    ],
    ]
```

### To Get All Pantry Requests:

**Request Method:** GET

**Path:** /pantries-requests/

Request:

```
Make sure to hit execute.

```

Response:

```json
[
  {
    "id": 0,
    "pantry": 0,
    "date_created": "2023-07-26",
    "additional": "string",
    "visitor": "string",
    "items": [0]
  }
]
```

### To Create a Pantry Request:

**Request Method:** POST

**Path:** /pantry-requests/

Request:

```json
{
  "pantry": 1,
  "additional": "Pantry Need's cleaning",
  "visitor": "MA",
  "items": [1, 2, 3]
}
```

Response:

```json
{
  "id": 1,
  "pantry": 1,
  "date_created": "2023-07-26",
  "additional": "Pantry Need's cleaning",
  "visitor": "MA",
  "items": [1, 2, 3]
}
```

## To Interact with the Neighborhood Endpoints

Methods: GET, POST, GET, PUT, DELETE

- To Get All Neighborhoods:

Input:

Click _Execute_.

Output:

```json
[
  {
    "id": 1,
    "neighborhood_name": "string"
  },
  {
    "id": 2,
    "neighborhood_name": "string"
  }
]
```

- To Create a Neighborhood (protected):

Input:

```json
{
  "neighborhood_name": "string"
}
```

Output:

```json
{
  "id": 0,
  "neighborhood_name": "string"
}
```

- To Get One Neighborhood:

Input:

Enter the Neighborhood ID number and click _Enter_.

Output:

```json
{
  "id": 0,
  "neighborhood_name": "string"
}
```

- To Update a Neighborhood (protected):

Input:

Enter the ID of the neighborhood you
wish to update. Then, update the name and click _Enter_:

```json
{
  "neighborhood_name": "updated_string"
}
```

Output:

```json
{
  "id": 0,
  "neighborhood_name": "updated_string"
}
```

- To Delete a Neighborhood (protected):

Input:

Enter the ID of the neighborhood you
wish to update. Then, click _Execute_.

Output:

```json
true
```

## To Interact with the Food Status Endpoints

Methods: GET, POST, GET, PUT, DELETE

- To Get All Food Statuses:

Input:

Click _Execute_.

Output:

```json
[
  {
    "id": 1,
    "food_status_name": "nearly empty"
  },
  {
    "id": 2,
    "food_status_name": "room for more"
  },
  {
    "id": 3,
    "food_status_name": "pretty full"
  },
  {
    "id": 4,
    "food_status_name": "overflowing"
  }
]
```

- To Create a Food Status (protected):

Input:

```json
{
  "food_status_name": "string"
}
```

Output:

```json
{
  "id": 0,
  "food_status_name": "string"
}
```

- To Get One Food Status:

Input:

Enter the food status ID number and click _Enter_.

Output:

```json
{
  "id": 0,
  "food_status_name": "string"
}
```

- To Update a Food Status (protected):

Input:

Enter the ID of the food status you
wish to update. Then, update the name and click _Enter_:

```json
{
  "food_status_name": "updated_string"
}
```

Output:

```json
{
  "id": 0,
  "food_status_name": "updated_string"
}
```

- To Delete a Food Status (protected):

Input:

Enter the ID of the neighborhood you
wish to update. Then, click _Execute_.

Output:

```json
true
```

## To Interact with the Postal Code Endpoints

Methods: GET, POST, GET, PUT, DELETE

- To Get All Postal Codes:

Input:

Click _Execute_.

Output:

```json
[
  {
    "id": 0,
    "postal_code": "string",
    "neighborhood_id": 0
  }
]
```

- To Create a Postal Code (protected):

Input:

```json
{
  "postal_code": "string",
  "neighborhood_id": 0
}
```

Output:

```json
{
  "id": 0
  "postal_code": "string",
  "neighborhood_id": 0
}

```

- To Get One Postal Code:

Input:

Enter the postal code ID number and click _Enter_.

Output:

```json
{
  "id": 0,
  "postal_code": "string",
  "neighborhood_id": 0
}
```

- To Update a Postal Code (protected):

Input:

Enter the ID of the postal code you
wish to update. Then, update the code and/or neighborhood ID and click _Enter_:

```json
{
  "postal_code": "updated_string",
  "neighborhood_id": 0
}
```

Output:

```json
{
  "id": 0,
  "postal_code": "updated_string",
  "neighborhood_id": 0
}
```

- To Delete a Postal code (protected):

Input:

Enter the ID of the postal code you
wish to update. Then, click _Execute_.

Output:

```json
true
```

## To Interact With The Items End-Points

The Items endpoints are used with requests and check-ins for fridges and pantries. The initial items are populated with an SQL table migration.

```SQL
[
        # "UP" and "INSERT" SQL statement
        """
        CREATE TABLE items (
            id serial not null primary key,
            name varchar(200) not null
        );

        INSERT INTO items (
            name
            )
            VALUES
                (
                'water/drinks'
                ),
                (
                'sanitary items'
                ),
                (
                'prepared meals'
                ),
                (
                'pantry goods'
                ),
                (
                'cheese'
                ),
                (
                'frozen good'
                ),
                (
                'plasticware'
                ),
                (
                'fruit'
                ),
                (
                'meat'
                ),
                (
                'ice packs'
                ),
                (
                'vegetables'
                ),
                (
                'milk'
                ),
                (
                'grocery bags'
                ),
                (
                'bread'
                ),
                (
                'pastries'
                );
        """,
        # "Down" SQL statement
        """
        DELETE from items;
        DROP TABLE items;
        """,
    ]
```

### Get All Items:

**Request Method:** GET

**Path:** /items/

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
[
  {
    "id": 1,
    "name": "string"
  },
  {
    "id": 2,
    "name": "string"
  }
]
```

### Get One Item:

**Request Method:** GET

**Path:** /items/{item_id}/

**Path Parameters:** item_id: int

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "id": 0,
  "name": "string"
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Create an Item (Protected Endpoint)

**Request Method:** POST

**Path:** /items/

#### Request Body:

```json
{
  "name": "string"
}
```

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "id": 0,
  "name": "string"
}
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to create an item."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Update One Item (Protected Endpoint)

**Request Method:** PUT

**Path:** /items/{item_id}/

**Path Parameters:** item_id: int

#### Request Body:

```json
{
  "name": "string"
}
```

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "id": 0,
  "name": "string"
}
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to update an item."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Delete One Item (Protected Endpoint)

**Request Method:** DELETE

**Path:** /items/{item_id}/

**Path Parameters:** item_id: int

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "message": "Item successfully deleted."
}
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to delete an item."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Fridges endpoints

## Create a new Fridge

- Endpoint path: /api/fridges/new/
- Endpoint method: POST
- Headers:
  - Authorization: Bearer token
- Request body:
  ```json
  {
    "fridge_name": str,
    "fridge_picture": str,
    "neighborhood": int,
    "location": str,
    "hours_start": time,
    "hours_end": time
  }
  ```
- Response: Create a new fridge
- Response shape:

```json
{
  "success": boolean,
  "message": string
}
```

## Get A List of Listings

- Endpoint path: /api/fridges/
- Endpoint method: GET
- Query parameters: None
- Response: A list of listings
- Response shape (JSON):

```json
  "fridge_listings": [{
    "id": int,
    "fridge_name": str,
    "fridge_picture": str,
    "neighborhood": int,
    "location": str,
    "hours_start": time,
    "hours_end": time
  }, ...]
```

## Get a specific Fridge

- Endpoint path: /api/fridges/id
- Endpoint method: GET
- Query parameters: id
- Response: A specific fridge
- Response shape (JSON):

```json
  {
    "id": int,
    "fridge_name": str,
    "fridge_picture": str,
    "neighborhood": int,
    "location": str,
    "hours_start": time,
    "hours_end": time
  }
```

## Update a Fridge

- Endpoint path: /api/fridges/id/edit
- Endpoint method: PUT
- Headers:
  - Authorization: Bearer token
- Response: Fridge details with updated information
- Request body (JSON):
  ```json
  {
    "id": int,
    "fridge_name": str,
    "fridge_picture": str,
    "neighborhood": int,
    "location": str,
    "hours_start": time,
    "hours_end": time
  }
  ```
- Response: An indication of success or failure
- Response shape:

  ```json
  {
    "id": int,
    "fridge_name": str,
    "fridge_picture": str,
    "neighborhood": int,
    "location": str,
    "hours_start": time,
    "hours_end": time
  }
  ```

  ## Delete a fridge

- Endpoint path: /api/fridges/<id>/delete
- Endpoint method: DELETE
- Query parameters: <id>
- Headers:
  - Authorization: Bearer token
- Response: An indication of success or failure
- Response shape:
  ```JSON
  {
  "success": boolean,
  "message": string
  }
  ```

## To Interact With The Check-In Reasons End-Points

The Reasons endpoints are for CRUD operations on Check-in Reasons, which are to be used in the Check-in forms. There are some prepopulated options, and the option to add more as seen fit.

```SQL
 [
        # "Up" SQL statement
        """
        CREATE TABLE check_in_reasons (
            id serial not null primary key,
            name varchar(100) not null
        );
        INSERT INTO check_in_reasons (name)
        VALUES ('Adding Food'), ('Collecting Food'), ('Cleaning the Fridge/Pantry'), ('Checking Stock Levels');
        """,
        # "Down" SQL statement
        """
        DROP TABLE check_in_reasons;
        """,
    ]
```

### Get All Reasons (Unprotected Endpoint)

**Request Method:** GET

**Path:** /api/reasons/

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
[
  {
    "id": 1,
    "name": "Adding Food"
  },
  {
    "id": 2,
    "name": "Collecting Food"
  }
]
```

### Get One Reason (Unprotected Endpoint)

**Request Method:** GET

**Path:** /api/reasons/{reason_id}/

**Path Parameters:** reason_id: int

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "id": 1,
  "name": "Adding Food"
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Create a Reason

**Request Method:** POST

**Path:** /api/reasons

#### Request Body:

```json
{
  "name": "string"
}
```

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "id": 0,
  "name": "string"
}
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to create a reason."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Update One Reason (Protected Endpoint)

**Request Method:** PUT

**Path:** /api/reasons/{reasons_id}/

**Path Parameters:** reasons_id: int

#### Request Body:

```json
{
  "name": "string"
}
```

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "id": 0,
  "name": "string"
}
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to update a reason."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Delete One reason (Protected Endpoint)

**Request Method:** DELETE

**Path:** /api/reasons/{reason_id}/

**Path Parameters:** user_id: int

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "id": 0,
  "name": "string"
}
```

**Status:** 401 - Unauthorized

```json
{
  "message": "Sign in to delete a check-in reason."
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

## To Interact With The Fridge Check-Ins End-Points

The Fridge Checkin endpoints are for just Get All, Get one, and Create operations for fridge checkins, which are to be used as the Check-in forms.

In the databases, three tables were created to track the many to many relationships that would exist with the reasons and the items tables.

```SQL
 [
        """
        CREATE TABLE fridge_checkins (
            id SERIAL PRIMARY KEY not NULL,
            fridge INT REFERENCES fridges (id) not NULL,
            date_time TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
            clean_status INT REFERENCES clean_statuses (id),
            food_status INT references food_statuses (id),
            picture TEXT,
            additional TEXT,
            visitor TEXT
           );
        """,
        """
        DROP TABLE fridge_checkins;
        """,
    ],
[
        """
        CREATE TABLE fridge_checkin_reasons (
        fridge_checkin_id INT NOT NULL,
        reason_id INT NOT NULL,
        FOREIGN KEY (fridge_checkin_id) REFERENCES fridge_checkins (id),
        FOREIGN KEY (reason_id) REFERENCES check_in_reasons (id),
        PRIMARY KEY (fridge_checkin_id, reason_id)
        );
        """,
        """
        DROP TABLE fridge_checkin_reasons;
        """,
    ],
    [
        """
        CREATE TABLE fridge_checkin_items (
        fridge_checkin_id INT NOT NULL,
        item_id INT NOT NULL,
        FOREIGN KEY (fridge_checkin_id) REFERENCES fridge_checkins (id),
        FOREIGN KEY (item_id) REFERENCES items (id),
        PRIMARY KEY (fridge_checkin_id, item_id)
        );
        """,
        """
        DROP TABLE fridge_checkin_items;
        """,
    ]
```

### Get All Fridge Check-ins (Protected Endpoint)

**Request Method:** GET

**Path:** /api/fridgecheckins/

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
[
  {
    "id": 0,
    "fridge": 0,
    "date_time": "2023-07-27T18:01:40.997Z",
    "reasons": [0],
    "clean_status": 0,
    "food_status": 0,
    "stocked_items": [0],
    "picture": "string",
    "additional": "string",
    "visitor": "string"
  }
]
```

### Get One Fridge Checkin (Unprotected Endpoint)

**Request Method:** GET

**Path:** /api/fridgecheckins/{id}/

**Path Parameters:** id: int

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
[
  {
    "id": 0,
    "fridge": 0,
    "date_time": "2023-07-27T18:02:58.758Z",
    "reasons": [0],
    "clean_status": 0,
    "food_status": 0,
    "stocked_items": [0],
    "picture": "string",
    "additional": "string",
    "visitor": "string"
  }
]
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

### Create a Fridge Checkin (Unprotected)

**Request Method:** POST

**Path:** /api/fridgecheckins

#### Request Body:

```json
{
  "fridge": 0,
  "reasons": [0],
  "clean_status": 0,
  "food_status": 0,
  "stocked_items": [0],
  "picture": "string",
  "additional": "string",
  "visitor": "string"
}
```

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "id": 0,
  "fridge": 0,
  "date_time": "2023-07-27T19:09:17.862Z",
  "reasons": [0],
  "clean_status": 0,
  "food_status": 0,
  "stocked_items": [0],
  "picture": "string",
  "additional": "string",
  "visitor": "string"
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

## To Interact With The Fridge Requests End-Points

The Fridge Checkin endpoints are for just Get All and Create operations for fridge requests, which are to be used as the Fridge item request forms.

In the databases, two tables were created to track the many to many relationships that would exist with the items table.

```SQL
[
        """
        CREATE TABLE fridge_requests(
            id serial not null primary key,
            fridge int REFERENCES fridges (id),
            date_created date not null DEFAULT CURRENT_DATE,
            additional varchar(500),
            visitor varchar(100)
        );
        """,
        """
        DROP TABLE fridge_requests;
        """,
    ],
    [
        """
        CREATE TABLE fridge_request_items (
        fridge_request_id INT NOT NULL,
        item_id INT NOT NULL,
        FOREIGN KEY (fridge_request_id) REFERENCES fridge_requests (id),
        FOREIGN KEY (item_id) REFERENCES items (id),
        PRIMARY KEY (fridge_request_id, item_id)
        );
        """,
        """
        DROP TABLE fridge_request_items;
        """,
    ]
```

### Get All Fridge Requests (Protected Endpoint)

**Request Method:** GET

**Path:** /fridge-requests/

#### Request Body:

_There is not a request body for this endpoint._

#### Responses:

**Status:** 200 - Successful Response

```json
[
  {
    "id": 0,
    "fridge": 0,
    "date_created": "2023-07-27",
    "additional": "string",
    "visitor": "string",
    "items": [0]
  }
]
```

**Status:** 200 - Unauthorized Response

```json
{
  "message": "Sign in to view fridge requests."
}
```

### Create a Fridge Request (Unprotected)

**Request Method:** POST

**Path:** /fridge-requests/

#### Request Body:

```json
{
  "fridge": 0,
  "additional": "string",
  "visitor": "string",
  "items": [0]
}
```

#### Responses:

**Status:** 200 - Successful Response

```json
{
  "id": 0,
  "fridge": 0,
  "date_created": "2023-07-27",
  "additional": "string",
  "visitor": "string",
  "items": [0]
}
```

**Status:** 422 - Validation Error

```json
{
  "detail": [
    {
      "loc": ["string", 0],
      "msg": "string",
      "type": "string"
    }
  ]
}
```

```

```
