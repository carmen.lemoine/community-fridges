Journal File

6/29
Today we added all our database requirements to our docker compose as a team and setup pg admin and all the appropriate volumes.

6/30
We began creating and working on issues and endpoints. We started making our migrations, I began with "checkin reasons" in order to prepare for the fridge check ins.

7/10
I finished all the relevant endpoints for the checkin reasons and I began setting up the migrations table for the fridge checkins.

7/11
I began setting up all the endpoints for the fridge check ins. The create is particularly difficult due to the lists capabilities that we want to be able to implement as a feature.

7/12
We figured out that we needed to set up extra tables for all the foreign keys in order to be able to related many to many relationships. Maria made some breakthroughs in her code and I was able to apply some of her methods into my own code. It was very difficult and ended up needing some help from the team, but ultimately I was very happy with how it turned out.

7/13
I was able to finish up all the back-end endpoints for checkins and made the merge request. Sam was able to test everything successfully and approved the merge request.

7/14
Today I began setting up the database and endpoints for the fridge item requests forms. With the solutions that we had found in the fridge checkins, I was able to relatively easily solve the ways I needed to set up the queries for this one, as we also needed to be able to input a list.
This closed up my part of the back end production.

7/17
Today our team began with front end production. We each chose which pages we wanted to build and assigned all the necessary pages. We also created all the right issues on Gitlab so that we can all make our branches for the front end. We went through front end auth as a team and got it working.

7/18
I began setting up my page for the fridge check in form. There's a lot of issues that I'm running into but I was able to set up the return statement and display the form. By the end of the day I got it working and now I can start putting together the data calls tomorrow.

7/19
Today I finished all the appropriate fetch calls and was able to get all the right information populating and get it to populate in the right formats. I had some trouble getting the list items to show up and such and also getting them to push correctly to the submit function, but I think I pretty much got everything working.

7/20
Today I started working on my front end page for the fridge item requests. There is some work to be done but it was a lot easier now that I went through the fridge checkin page so all this groundwork was already laid. Finally I also finished the pantry list page and it was not bad at all. That wraps up all my front end screens !

7/24
Over the weekend, I worked on a navigation bar for our page end setting up some basic home page and stuff. It wasn't bad at all. Today we're ironing out kinks and I'm working on my unit tests for the end of our project. The test kicked my butt (sad face).

7/25
Today we deployed ! It was a long arduous process from sun up to sun down and we finally got it working! There was a lot of issues along the way but we solved them as a team and now our site is live! Exciting!

7/26
We presented to the instructors and we are just wrapping up readme to complete our project !
