from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.fridge_requests import (
    Error,
    FridgeRequestIn,
    FridgeRequestRepository,
    FridgeRequestOut,
)
from authenticator import authenticator

router = APIRouter()


@router.post(
    "/fridge-requests/",
    tags=["Fridge Requests"],
    response_model=Union[FridgeRequestOut, Error],
)
def create_a_fridge_request(
    request: FridgeRequestIn,
    response: Response,
    repo: FridgeRequestRepository = Depends(),
) -> FridgeRequestOut | Error:
    result = repo.create(request)
    if result is None:
        response.status_code = 404
        return Error(message="Unable to create fridge request.")
    return result


@router.get(
    "/fridge-requests/",
    tags=["Fridge Requests"],
    response_model=Union[List[FridgeRequestOut], Error],
)
def get_all_fridge_requests(
    response: Response,
    repo: FridgeRequestRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to view fridge requests.")
    return repo.get_all()
