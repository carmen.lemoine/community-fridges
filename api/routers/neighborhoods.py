from fastapi import APIRouter, Depends, Response
from typing import List, Union, Optional
from queries.neighborhoods import (
    NeighborhoodIn,
    NeighborhoodOut,
    NeighborhoodRepo,
    Error,
)
from authenticator import authenticator

router = APIRouter()


@router.get(
    "/neighborhoods",
    tags=["Neighborhoods"],
    response_model=Union[List[NeighborhoodOut], Error],
)
def get_all_neighborhoods(repo: NeighborhoodRepo = Depends()):
    return repo.get_all()


@router.get(
    "/neighborhoods/{neighborhood_id}",
    tags=["Neighborhoods"],
    response_model=Optional[NeighborhoodOut],
)
def get_one_neighborhood(
    neighborhood_id: int,
    response: Response,
    repo: NeighborhoodRepo = Depends(),
) -> NeighborhoodOut:
    neighborhood = repo.get_one(neighborhood_id)
    if neighborhood is None:
        response.status_code = 404
    return neighborhood


@router.post(
    "/neighborhoods",
    tags=["Neighborhoods"],
    response_model=Union[NeighborhoodOut, Error],
)
def create_neighborhood(
    neighborhood: NeighborhoodIn,
    response: Response,
    repo: NeighborhoodRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> NeighborhoodOut | Error:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to create a neighborhood.")
    new_neighborhood = repo.create(neighborhood)
    if new_neighborhood is None:
        response.status_code = 400
        return Error(message="Unable to create neighborhood.")
    return new_neighborhood


@router.put(
    "/neighborhoods/{neighborhood_id}",
    tags=["Neighborhoods"],
    response_model=Union[NeighborhoodOut, Error],
)
def update_neighborhood(
    neighborhood_id: int,
    neighborhood: NeighborhoodIn,
    response: Response,
    repo: NeighborhoodRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data)
) -> Union[NeighborhoodOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to update a neighborhood.")
    result = repo.update(neighborhood_id, neighborhood)
    if "message" in result:
        response.status_code = 400
    else:
        response.status_code = 200
        return result


@router.delete(
    "/neighborhoods/{neighborhood_id}",
    tags=["Neighborhoods"],
    response_model=bool,
)
def delete_neighborhood(
    neighborhood_id: int,
    response: Response,
    repo: NeighborhoodRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data)
) -> bool:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to delete a neighborhood.")
    return repo.delete(neighborhood_id)
