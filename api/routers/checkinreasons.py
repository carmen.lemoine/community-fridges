from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.checkinreasons import (
    CheckInReasonsIn,
    ReasonsRepo,
    CheckInReasonsOut,
    Error,
)
from authenticator import authenticator


router = APIRouter()


@router.post(
    "/api/reasons",
    tags=["Check-In Reasons"],
    response_model=Union[CheckInReasonsOut, Error],
)
def create_checkinreasons(
    reason: CheckInReasonsIn,
    response: Response,
    repo: ReasonsRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> CheckInReasonsOut | Error:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to create a reason.")
    result = repo.create(reason)
    if result is None:
        response.status_code = 404
        result = Error(message="Unable to create reason.")
    return result


@router.get(
    "/api/reasons",
    tags=["Check-In Reasons"],
    response_model=Union[List[CheckInReasonsOut], Error],
)
def get_all_reasons(repo: ReasonsRepo = Depends()):
    return repo.get_all()


@router.get("/api/reasons/{reason_id}/", tags=["Check-In Reasons"])
async def get_one_reason(
    reason_id: int,
    reasons: ReasonsRepo = Depends(),
) -> CheckInReasonsOut:
    reason = reasons.get_one(reason_id)
    return reason


@router.put("/api/reasons/{reason_id}/", tags=["Check-In Reasons"])
async def update_one_reason(
    reason_id: int,
    reason: CheckInReasonsIn,
    response: Response,
    repo: ReasonsRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[CheckInReasonsOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to update a reason.")
    result = repo.update(reason_id, reason)
    if "message" in result:
        response.status_code = 400
    else:
        response.status_code = 200
    return result


@router.delete("/api/reasons/{reason_id}/", tags=["Check-In Reasons"])
async def delete_one_reason(
    reason_id: int,
    response: Response,
    repo: ReasonsRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[CheckInReasonsOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to delete a check-in reason.")
    result = repo.delete(reason_id)
    response.status_code = result.get("status", 500)
    if response.status_code != 200:
        response.status_code = 404
        return Error(message=result["message"])
    return result
