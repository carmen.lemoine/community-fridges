from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.fridges import (
    Error,
    fridgesIn,
    fridgesOut,
    fridgesRepository,
)
from authenticator import authenticator

router = APIRouter()


@router.post(
    "/fridges", tags=["Fridges"], response_model=Union[fridgesOut, Error]
)
async def create_fridge(
    fridge: fridgesIn,
    response: Response,
    repo: fridgesRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to create fridges")

    result = repo.create(fridge)
    if result is None:
        response.status_code = 400
        result = Error(message="Unable to create new fridge")
    else:
        return result


@router.get(
    "/fridges/{fridge_id}",
    tags=["Fridges"],
)
async def get_a_fridge(
    fridge_id: int,
    fridges: fridgesRepository = Depends(),
) -> fridgesOut:
    fridge = fridges.get_a_fridge(fridge_id)
    return fridge


@router.get(
    "/fridges", tags=["Fridges"], response_model=Union[List[fridgesOut], Error]
)
async def get_all(
    response: Response,
    repo: fridgesRepository = Depends(),
):
    result = repo.get_all()
    if result is None:
        response.status_code = 400
        result = Error(message="Unable to get all fridges")
    return result


@router.put(
    "/fridges/{fridge_id}",
    tags=["Fridges"],
    response_model=Union[fridgesOut, Error],
)
def update_fridge(
    fridge_id: int,
    fridge: fridgesIn,
    response: Response,
    repo: fridgesRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[fridgesOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to update a fridge.")
    result = repo.update_fridge(fridge_id, fridge)
    if "message" in result:
        response.status_code = 400
    else:
        response.status_code = 200
    return result


@router.delete("/fridges/{fridge_id}", tags=["Fridges"], response_model=bool)
async def delete_a_fridge(
    listing_id: int,
    response: Response,
    repo: fridgesRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to delete a fridge")

    result = repo.delete_a_fridge(listing_id)
    if result is False:
        response.status_code = (400,)
        result = Error(message="unable to process delete request")

    return result
