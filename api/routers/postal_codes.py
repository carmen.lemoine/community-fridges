from fastapi import APIRouter, Depends, Response
from typing import List, Union, Optional
from queries.postal_codes import (
    PostalCodeIn,
    PostalCodeOut,
    PostalCodeRepo,
    Error,
)
from authenticator import authenticator

router = APIRouter()


@router.get(
    "/postal_codes",
    tags=["Postal Codes"],
    response_model=Union[List[PostalCodeOut], Error],
)
def get_all_codes(repo: PostalCodeRepo = Depends()):
    return repo.get_all()


@router.get(
    "/postal_codes/{postal_code_id}",
    tags=["Postal Codes"],
    response_model=Optional[PostalCodeOut],
)
def get_one_code(
    postal_code_id: int,
    response: Response,
    repo: PostalCodeRepo = Depends(),
) -> PostalCodeOut:
    code = repo.get_one(postal_code_id)
    if code is None:
        response.status_code = 404
    return code


@router.post(
    "/postal_codes",
    tags=["Postal Codes"],
    response_model=Union[PostalCodeOut, Error],
)
def create_code(
    code: PostalCodeIn,
    response: Response,
    repo: PostalCodeRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> PostalCodeOut | Error:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to create a postal code.")
    new_code = repo.create(code)
    if new_code is None:
        response.status_code = 400
        return Error(message="Unable to create postal code.")
    return new_code


@router.put(
    "/postal_codes/{postal_code_id}",
    tags=["Postal Codes"],
    response_model=Union[PostalCodeOut, Error],
)
def update_code(
    postal_code_id: int,
    code: PostalCodeIn,
    response: Response,
    repo: PostalCodeRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[PostalCodeOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to update a postal code.")
    result = repo.update(postal_code_id, code)
    if result is None:
        response.status_code = 400
        return Error(message="Unable to update postal code.")
    return result


@router.delete(
    "/postal_codes/{postal_code_id}",
    tags=["Postal Codes"],
    response_model=bool,
)
def delete_code(
    postal_code_id: int,
    response: Response,
    repo: PostalCodeRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> bool:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to delete a postal code.")
    return repo.delete(postal_code_id)
