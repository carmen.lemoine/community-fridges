from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.food_status import (
    FoodStatusIn,
    FoodStatusOut,
    FoodStatusRepo,
    Error,
)
from authenticator import authenticator

router = APIRouter()


@router.get(
    "/food_statuses",
    tags=["Food Statuses"],
    response_model=Union[List[FoodStatusOut], Error],
)
def get_all_food_statuses(repo: FoodStatusRepo = Depends()):
    return repo.get_all()


@router.get(
    "/food_statuses/{food_status_id}",
    tags=["Food Statuses"],
)
def get_one_food_status(
    food_status_id: int,
    response: Response,
    repo: FoodStatusRepo = Depends(),
) -> FoodStatusOut:
    status = repo.get_one(food_status_id)
    if status is None:
        response.status_code = 404
    return status


@router.post(
    "/food_statuses",
    tags=["Food Statuses"],
    response_model=Union[FoodStatusOut, Error],
)
def create_food_status(
    food_status: FoodStatusIn,
    response: Response,
    repo: FoodStatusRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> FoodStatusOut | Error:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to create a food status.")
    new_status = repo.create(food_status)
    if new_status is None:
        response.status_code = 400
        return Error(message="Unable to create food status.")
    return new_status


@router.put(
    "/food_statuses/{food_status_id}",
    tags=["Food Statuses"],
    response_model=Union[FoodStatusOut, Error],
)
def update_food_status(
    food_status_id: int,
    food_status: FoodStatusIn,
    response: Response,
    repo: FoodStatusRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[FoodStatusOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to update food status.")
    result = repo.update(food_status_id, food_status)
    if result is None:
        response.status_code = 400
    return result


@router.delete(
    "/food_statuses/{food_status_id}",
    tags=["Food Statuses"],
    response_model=bool,
)
def delete_food_status(
    food_status_id: int,
    response: Response,
    repo: FoodStatusRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> bool:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to delete food status.")
    return repo.delete(food_status_id)
