from fastapi.testclient import TestClient
from main import app


client = TestClient(app)


def test_read_health_status():
    response = client.get("/health")
    assert response.status_code == 200
    assert response.json() == {"status": 200}


def test_read_health_status_with_error(monkeypatch):
    def mock_get(self):
        raise Exception("Test exception")

    monkeypatch.setattr("queries.healthcheck.HealthStatusRepo.get", mock_get)

    response = client.get("/health")
    assert response.status_code == 500
    assert response.json() == {"detail": "Test exception"}
