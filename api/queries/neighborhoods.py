from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class NeighborhoodIn(BaseModel):
    neighborhood_name: str


class NeighborhoodOut(BaseModel):
    id: int
    neighborhood_name: str


class Error(BaseModel):
    message: str


class NeighborhoodRepo:
    def neighborhood_in_to_out(self, id: int, neighborhood: NeighborhoodIn):
        old_data = neighborhood.dict()
        return NeighborhoodOut(id=id, **old_data)

    def record_to_neighborhood_out(self, record):
        return NeighborhoodOut(
            id=record[0],
            neighborhood_name=record[1],
        )

    def get_all(self) -> Union[List[NeighborhoodOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, neighborhood_name
                        FROM neighborhoods
                        """
                    )
                    return [
                        self.record_to_neighborhood_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve all neighborhoods."}

    def create(self, neighborhood: NeighborhoodIn) -> NeighborhoodOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO neighborhoods (neighborhood_name)
                        VALUES (%s)
                        RETURNING id;
                        """,
                        [neighborhood.neighborhood_name],
                    )
                    id = result.fetchone()[0]
                    return self.neighborhood_in_to_out(id, neighborhood)
        except Exception as e:
            print(e)
            return {"message": "Could not create neighborhood."}

    def update(
        self, neighborhood_id: int, neighborhood: NeighborhoodIn
    ) -> Union[NeighborhoodOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE neighborhoods
                        SET neighborhood_name = %s
                        WHERE id = %s
                        """,
                        (neighborhood.neighborhood_name, neighborhood_id),
                    )
                    return self.neighborhood_in_to_out(
                        neighborhood_id, neighborhood
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not update neighborhood."}

    def delete(self, neighborhood_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM neighborhoods
                        WHERE id = %s
                        """,
                        [neighborhood_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_one(self, neighborhood_id: int) -> Optional[NeighborhoodOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , neighborhood_name
                        FROM neighborhoods
                        WHERE id = %s
                        """,
                        [neighborhood_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_neighborhood_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not find neighborhood."}
