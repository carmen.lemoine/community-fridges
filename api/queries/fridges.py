from datetime import time, date
from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional, Union


class Error(BaseModel):
    message: str


class Neighborhood(BaseModel):
    id: int
    neighborhood_name: str


class fridgesIn(BaseModel):
    active: bool
    fridge_name: str
    fridge_picture: str
    neighborhood: int
    location: str
    hours_start: time
    hours_end: time
    date_created: date


class fridgesOutNoNeighborhood(BaseModel):
    id: int
    active: bool
    fridge_name: str
    fridge_picture: str
    location: str
    hours_start: time
    hours_end: time
    date_created: date


class fridgesOut(BaseModel):
    id: int
    active: bool
    fridge_name: str
    fridge_picture: str
    neighborhood: Neighborhood
    location: str
    hours_start: time
    hours_end: time
    date_created: date


class fridgesRepository:
    def create(self, fridge: fridgesIn) -> fridgesOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                        INSERT INTO fridges
                            (active,
                            fridge_name,
                            fridge_picture,
                            neighborhood,
                            location,
                            hours_start,
                            hours_end,
                            date_created
                            )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;

                        """,
                    [
                        fridge.active,
                        fridge.fridge_name,
                        fridge.fridge_picture,
                        fridge.neighborhood,
                        fridge.location,
                        fridge.hours_start,
                        fridge.hours_end,
                        fridge.date_created,
                    ],
                )
                id = db.fetchone()[0]
                if id is None:
                    return None

                with conn.cursor() as db_neighborhood:
                    db_neighborhood.execute(
                        """
                    SELECT
                        id,
                        neighborhood_name
                        FROM neighborhoods
                        WHERE id = %s;
                    """,
                        [fridge.neighborhood],
                    )

                    result = db_neighborhood.fetchone()

                    neighborhood = {
                        "id": result[0],
                        "neighborhood_name": result[1],
                    }
                    print(neighborhood)

                return fridgesOut(
                    id=id,
                    active=fridge.active,
                    fridge_name=fridge.fridge_name,
                    fridge_picture=fridge.fridge_picture,
                    neighborhood=Neighborhood(
                        id=result[0], neighborhood_name=result[1]
                    ),
                    location=fridge.location,
                    hours_start=fridge.hours_start,
                    hours_end=fridge.hours_end,
                    date_created=fridge.date_created,
                )

    def get_a_fridge(self, fridge_id) -> Optional[fridgesOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            fridges.id,
                            fridges.active,
                            fridges.fridge_name,
                            fridges.fridge_picture,
                            neighborhoods.id,
                            neighborhoods.neighborhood_name,
                            fridges.location,
                            fridges.hours_start,
                            fridges.hours_end,
                            fridges.date_created
                        FROM fridges
                        INNER JOIN neighborhoods ON fridges.neighborhood = neighborhoods.id
                        WHERE fridges.id = %s
                        """,
                        [fridge_id],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None

                    return fridgesOut(
                        id=record[0],
                        active=record[1],
                        fridge_name=record[2],
                        fridge_picture=record[3],
                        neighborhood=Neighborhood(
                            id=record[4], neighborhood_name=record[5]
                        ),
                        location=record[6],
                        hours_start=record[7],
                        hours_end=record[8],
                        date_created=record[9],
                    )

        except Exception as e:
            print(e)
            return {"message": "Could not return a fridge"}

    def get_all(self) -> Union[Error, List[fridgesOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            fridges.id,
                            fridges.active,
                            fridges.fridge_name,
                            fridges.fridge_picture,
                            neighborhoods.id,
                            neighborhoods.neighborhood_name,
                            fridges.location,
                            fridges.hours_start,
                            fridges.hours_end,
                            fridges.date_created
                        FROM fridges
                        INNER JOIN neighborhoods ON fridges.neighborhood = neighborhoods.id
                        ORDER BY fridges.id
                        """
                    )
                    return [
                        fridgesOut(
                            id=record[0],
                            active=record[1],
                            fridge_name=record[2],
                            fridge_picture=record[3],
                            neighborhood=Neighborhood(
                                id=record[4], neighborhood_name=record[5]
                            ),
                            location=record[6],
                            hours_start=record[7],
                            hours_end=record[8],
                            date_created=record[9],
                        )
                        for record in db
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not return all fridges"}

    def update_fridge(
        self, fridge_id: int, fridge: fridgesIn
    ) -> Union[fridgesOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE fridges
                        SET active=%s,
                            fridge_name=%s,
                            fridge_picture=%s,
                            neighborhood=%s,
                            location=%s,
                            hours_start=%s,
                            hours_end=%s
                            WHERE id = %s
                        """,
                        [
                            fridge.active,
                            fridge.fridge_name,
                            fridge.fridge_picture,
                            fridge.neighborhood,
                            fridge.location,
                            fridge.hours_start,
                            fridge.hours_end,
                            fridge_id,
                        ],
                    )
                    id = db.rowcount
                    print(id)
                    if id is None:
                        return None

                with conn.cursor() as db_neighborhood:
                    db_neighborhood.execute(
                        """
                    SELECT
                        id,
                        neighborhood_name
                        FROM neighborhoods
                        WHERE id = %s;
                    """,
                        [fridge.neighborhood],
                    )

                    result = db_neighborhood.fetchone()
                    print(result)
                    print(result[0])
                    print(result[1])
                    neighborhood = {
                        "id": result[0],
                        "neighborhood_name": result[1],
                    }
                    print(neighborhood)

                return fridgesOut(
                    id=id,
                    active=fridge.active,
                    fridge_name=fridge.fridge_name,
                    fridge_picture=fridge.fridge_picture,
                    neighborhood=Neighborhood(
                        id=result[0], neighborhood_name=result[1]
                    ),
                    location=fridge.location,
                    hours_start=fridge.hours_start,
                    hours_end=fridge.hours_end,
                    date_created=fridge.date_created,
                )
        except Exception as e:
            print(e)
            return {"message": "Could not update that fridge."}

    def delete_a_fridge(self, listing_id) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM fridges
                        WHERE id = %s
                        """,
                        [listing_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return {"message": "Could not delete a fridge"}

    def record_to_fridge_out(self, record):
        return fridgesOut(
            id=record[0],
            active=record[1],
            fridge_name=record[2],
            fridge_picture=record[3],
            neighborhood=Neighborhood(
                id=record[4], neighborhood_name=record[5]
            ),
            location=record[6],
            hours_start=record[7],
            hours_end=record[8],
            date_created=record[9],
        )

    def fridge_in_to_out(self, id: int, fridge: fridgesIn):
        old_data = fridge.dict()
        return fridgesOut(id=id, **old_data)
