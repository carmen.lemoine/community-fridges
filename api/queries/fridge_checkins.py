from pydantic import BaseModel
from queries.pool import pool
from typing import List, Union
from datetime import datetime


class Error(BaseModel):
    message: str


class FridgeCheckInsIn(BaseModel):
    fridge: int
    reasons: List[int]
    clean_status: int
    food_status: int
    stocked_items: List[int]
    picture: str
    additional: str
    visitor: str


class FridgeCheckInsOut(BaseModel):
    id: int
    fridge: int
    date_time: datetime
    reasons: List[int]
    clean_status: int
    food_status: int
    stocked_items: List[int]
    picture: str
    additional: str
    visitor: str


class FridgeCheckInsRepo:
    def get_all(self) -> Union[Error, List[FridgeCheckInsOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                        fridge,
                        date_time,
                        clean_status,
                        food_status,
                        picture,
                        additional,
                        visitor
                        FROM fridge_checkins
                        ORDER BY date_time DESC

                        """
                    )
                    checkins = []
                    for record in result:
                        with conn.cursor() as db_reasons:
                            db_reasons.execute(
                                """
                                SELECT reason_id
                                FROM fridge_checkin_reasons
                                WHERE fridge_checkin_id = %s
                                """,
                                [record[0]],
                            )
                            reasons = [
                                reason[0] for reason in db_reasons.fetchall()
                            ]
                        with conn.cursor() as db_items:
                            db_items.execute(
                                """
                                SELECT item_id
                                FROM fridge_checkin_items
                                WHERE fridge_checkin_id = %s
                                """,
                                [record[0]],
                            )
                            items = [item[0] for item in db_items.fetchall()]
                        checkins.append(
                            self.record_to_checkin_out(record, reasons, items)
                        )
                    return checkins
        except Exception as e:
            print(e)
            return {"message": "Could not get all checkins"}

    def get_all_checkins_for_fridge(
        self, id
    ) -> Union[Error, List[FridgeCheckInsOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                        fridge,
                        date_time,
                        clean_status,
                        food_status,
                        picture,
                        additional,
                        visitor
                        FROM fridge_checkins
                        where fridge = %s
                        ORDER BY date_time DESC

                        """,
                        [id],
                    )
                    checkins = []
                    for record in result:
                        with conn.cursor() as db_reasons:
                            db_reasons.execute(
                                """
                                SELECT reason_id
                                FROM fridge_checkin_reasons
                                WHERE fridge_checkin_id = %s
                                """,
                                [record[0]],
                            )
                            reasons = [
                                reason[0] for reason in db_reasons.fetchall()
                            ]
                        with conn.cursor() as db_items:
                            db_items.execute(
                                """
                                SELECT item_id
                                FROM fridge_checkin_items
                                WHERE fridge_checkin_id = %s
                                """,
                                [record[0]],
                            )
                            items = [item[0] for item in db_items.fetchall()]
                        checkins.append(
                            self.record_to_checkin_out(record, reasons, items)
                        )
                    return checkins
        except Exception as e:
            print(e)
            return {"message": "Could not get all checkins for this fridge."}

    def create(
        self, checkin: FridgeCheckInsIn
    ) -> Union[FridgeCheckInsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO fridge_checkins
                            (fridge, clean_status, food_status, picture, additional, visitor)
                        VALUES
                            (%s, %s, %s, %s, %s, %s)
                        RETURNING id, fridge, date_time, clean_status, food_status, picture, additional, visitor;
                        """,
                        [
                            checkin.fridge,
                            checkin.clean_status,
                            checkin.food_status,
                            checkin.picture,
                            checkin.additional,
                            checkin.visitor,
                        ],
                    )
                    record = result.fetchone()
                    for reason_id in checkin.reasons:
                        db.execute(
                            """
                            INSERT INTO fridge_checkin_reasons
                            (fridge_checkin_id, reason_id)
                            VALUES
                            (%s, %s)
                            """,
                            [
                                record[0],
                                reason_id,
                            ],
                        )
                    for item_id in checkin.stocked_items:
                        db.execute(
                            """
                            INSERT INTO fridge_checkin_items
                            (fridge_checkin_id, item_id)
                            VALUES
                            (%s, %s)
                            """,
                            [
                                record[0],
                                item_id,
                            ],
                        )
                    conn.commit()
                    return self.record_to_checkin_out(
                        record, checkin.reasons, checkin.stocked_items
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not create a checkin at this time."}

    def record_to_checkin_out(self, record, reasons, items):
        return FridgeCheckInsOut(
            id=record[0],
            fridge=record[1],
            date_time=record[2],
            clean_status=record[3],
            food_status=record[4],
            picture=record[5],
            additional=record[6],
            visitor=record[7],
            reasons=reasons,
            stocked_items=items,
        )

    def checkin_in_to_out(self, id: int, checkin: FridgeCheckInsIn):
        old_data = checkin.dict()
        return FridgeCheckInsOut(id=id, **old_data)
