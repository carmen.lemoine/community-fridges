from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class CleanStatusIn(BaseModel):
    clean_status_name: str


class CleanStatusOut(BaseModel):
    id: int
    clean_status_name: str


class Error(BaseModel):
    message: str


class CleanStatusRepo:
    def clean_status_in_to_out(self, id: int, clean_status: CleanStatusIn):
        old_data = clean_status.dict()
        return CleanStatusOut(id=id, **old_data)

    def record_to_clean_status_out(self, record):
        return CleanStatusOut(
            id=record[0],
            clean_status_name=record[1],
        )

    def create(self, clean_status: CleanStatusIn) -> CleanStatusOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO clean_statuses
                            (clean_status_name
                            )
                        VALUES
                            (%s)
                        RETURNING id;
                        """,
                        [clean_status.clean_status_name],
                    )

                    tuple = db.fetchone()
                    id = tuple[0]
                    old_data = clean_status.dict()

                    return CleanStatusOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not create a status"}

    def update(
        self, clean_status_id: int, clean_status: CleanStatusIn
    ) -> Union[CleanStatusOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE clean_statuses
                        SET clean_status_name = %s
                        WHERE id = %s
                        """,
                        (clean_status.clean_status_name, clean_status_id),
                    )
                    return self.clean_status_in_to_out(
                        clean_status_id, clean_status
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not update clean status"}

    def delete(self, clean_status_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM clean_statuses
                        WHERE id = %s
                        """,
                        [clean_status_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_one(self, clean_status_id: int) -> Optional[CleanStatusOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , clean_status_name
                        FROM clean_statuses
                        WHERE id = %s
                        """,
                        [clean_status_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_clean_status_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get clean status"}

    def get_all(self) -> Union[List[CleanStatusOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, clean_status_name
                        FROM clean_statuses
                        """
                    )
                    return [
                        self.record_to_clean_status_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all clean statuses"}
