from pydantic import BaseModel
from typing import List, Union
from queries.pool import pool
from datetime import datetime


class Error(BaseModel):
    message: str


class PantryCheckIn(BaseModel):
    pantry_id: int
    food_status: int
    clean_status: int
    reasons: List[int]
    stocked_items: List[int]
    photo: str
    Anything_else_details: str
    name_or_initials: str


class PantryCheckOut(BaseModel):
    id: int
    pantry_id: int
    date_time: datetime
    food_status: int
    clean_status: int
    reasons: List[int]
    stocked_items: List[int]
    photo: str
    Anything_else_details: str
    name_or_initials: str


class PantryCheckInsRepository:
    def create(self, pantry_check: PantryCheckIn) -> PantryCheckOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            INSERT INTO pantry_check_ins
                              (pantry_id, food_status, clean_status, photo, Anything_else_details, name_or_initials)
                            VALUES (%s, %s, %s, %s, %s, %s)
                            RETURNING id, pantry_id, date_time, food_status, clean_status, photo, Anything_else_details, name_or_initials;
                            """,
                        [
                            pantry_check.pantry_id,
                            pantry_check.food_status,
                            pantry_check.clean_status,
                            pantry_check.photo,
                            pantry_check.Anything_else_details,
                            pantry_check.name_or_initials,
                        ],
                    )
                    record = result.fetchone()
                    for reason_id in pantry_check.reasons:
                        db.execute(
                            """
                            INSERT INTO pantry_checkin_reasons
                            (pantry_checkin_id, reason_id)
                            VALUES
                            (%s, %s)
                            """,
                            [
                                record[0],
                                reason_id,
                            ],
                        )
                    for item_id in pantry_check.stocked_items:
                        db.execute(
                            """
                            INSERT INTO pantry_checkin_items
                            (pantry_checkin_id, item_id)
                            VALUES
                            (%s, %s)
                            """,
                            [
                                record[0],
                                item_id,
                            ],
                        )
                    conn.commit()
                    return self.record_to_pantry_request_out(
                        record,
                        pantry_check.reasons,
                        pantry_check.stocked_items,
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not create pantry check ins."}

    def get_all_checkins_for_pantry(
        self, id
    ) -> Union[Error, List[PantryCheckOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            SELECT id,
                            pantry_id,
                            date_time,
                            clean_status,
                            food_status,
                            photo,
                            Anything_else_details,
                            name_or_initials
                            FROM pantry_check_ins
                            where pantry_id = %s
                            ORDER BY date_time DESC

                            """,
                        [id],
                    )
                    checkins = []
                    for record in result:
                        with conn.cursor() as db_reasons:
                            db_reasons.execute(
                                """
                                    SELECT reason_id
                                    FROM pantry_checkin_reasons
                                    WHERE pantry_checkin_id = %s
                                    """,
                                [record[0]],
                            )
                            reasons = [
                                reason[0] for reason in db_reasons.fetchall()
                            ]
                        with conn.cursor() as db_items:
                            db_items.execute(
                                """
                                    SELECT item_id
                                    FROM pantry_checkin_items
                                    WHERE pantry_checkin_id = %s
                                    """,
                                [record[0]],
                            )
                            items = [item[0] for item in db_items.fetchall()]
                        checkins.append(
                            self.record_to_pantry_request_out(
                                record, reasons, items
                            )
                        )
                    return checkins
        except Exception as e:
            print(e)
            return {"message": "Could not get all checkins for this pantry."}

    def record_to_pantry_request_out(self, record, reasons, items):
        return PantryCheckOut(
            id=record[0],
            pantry_id=record[1],
            date_time=record[2],
            food_status=record[3],
            clean_status=record[4],
            photo=record[5],
            Anything_else_details=record[6],
            name_or_initials=record[7],
            reasons=reasons,
            stocked_items=items,
        )

    def pantry_check_in_to_out(self, id: int, pantry_check: PantryCheckIn):
        old_data = pantry_check.dict()
        return PantryCheckOut(id=id, **old_data)

    def get_all(self) -> Union[List[PantryCheckOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM pantry_check_ins
                        ORDER BY id
                        """
                    )
                    checkins = []
                    for record in result:
                        with conn.cursor() as db_reasons:
                            db_reasons.execute(
                                """
                                SELECT reason_id
                                FROM pantry_checkin_reasons
                                WHERE pantry_checkin_id = %s
                                """,
                                [record[0]],
                            )
                            reasons = [
                                reason[0] for reason in db_reasons.fetchall()
                            ]
                        with conn.cursor() as db_items:
                            db_items.execute(
                                """
                                SELECT item_id
                                FROM pantry_checkin_items
                                WHERE pantry_checkin_id = %s
                                """,
                                [record[0]],
                            )
                            items = [item[0] for item in db_items.fetchall()]
                        checkins.append(
                            self.record_to_pantry_request_out(
                                record, reasons, items
                            )
                        )
                    return checkins
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve all pantry check ins."}
