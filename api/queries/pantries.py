from datetime import time, date
from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional, Union


class Error(BaseModel):
    message: str


class Neighborhood(BaseModel):
    id: int
    neighborhood_name: str


class PantryIn(BaseModel):
    active: bool
    pantry_name: str
    pantry_picture: str
    neighborhood: int
    location: str
    hours_start: time
    hours_end: time
    date_created: date


class pantriesOutNoNeighborhood(BaseModel):
    id: int
    active: bool
    pantry_name: str
    pantry_picture: str
    neighborhood: Neighborhood
    location: str
    hours_start: time
    hours_end: time
    date_created: date


class PantryOut(BaseModel):
    id: int
    active: bool
    pantry_name: str
    neighborhood: Neighborhood
    pantry_picture: str
    location: str
    hours_start: time
    hours_end: time
    date_created: date


class PantryRepository:
    def get_a_pantry(self, pantry_id) -> Optional[PantryOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            pantries.id,
                            pantries.active,
                            pantries.pantry_name,
                            pantries.pantry_picture,
                            neighborhoods.id,
                            neighborhoods.neighborhood_name,
                            pantries.location,
                            pantries.hours_start,
                            pantries.hours_end,
                            pantries.date_created
                        FROM pantries
                        INNER JOIN neighborhoods ON pantries.neighborhood = neighborhoods.id
                        WHERE pantries.id = %s
                        """,
                        [pantry_id],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None

                    return PantryOut(
                        id=record[0],
                        active=record[1],
                        pantry_name=record[2],
                        pantry_picture=record[3],
                        neighborhood=Neighborhood(
                            id=record[4], neighborhood_name=record[5]
                        ),
                        location=record[6],
                        hours_start=record[7],
                        hours_end=record[8],
                        date_created=record[9],
                    )
        except Exception:
            return {"message": "Could not get that pantry"}

    def delete_a_pantry(self, listing_id) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM pantries
                        WHERE id = %s
                        """,
                        [listing_id],
                    )
                    return True
        except Exception:
            return {"message": "Could not delete that pantry"}

    def update_pantry(
        self, pantry_id: int, pantry: PantryIn
    ) -> Union[PantryOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE pantries
                        SET active = %s,
                            pantry_name = %s,
                            pantry_picture=%s,
                            neighborhood=%s,
                            location=%s,
                            hours_start=%s,
                            hours_end=%s
                            WHERE id = %s
                        """,
                        [
                            pantry.active,
                            pantry.pantry_name,
                            pantry.pantry_picture,
                            pantry.neighborhood,
                            pantry.location,
                            pantry.hours_start,
                            pantry.hours_end,
                            pantry_id,
                        ],
                    )
                    id = db.rowcount
                    if id is None:
                        return None

                with conn.cursor() as db_neighborhood:
                    db_neighborhood.execute(
                        """
                    SELECT
                        id,
                        neighborhood_name
                        FROM neighborhoods
                        WHERE id = %s;
                    """,
                        [pantry.neighborhood],
                    )
                    result = db_neighborhood.fetchone()

                return PantryOut(
                    id=id,
                    active=pantry.active,
                    pantry_name=pantry.pantry_name,
                    pantry_picture=pantry.pantry_picture,
                    neighborhood=Neighborhood(
                        id=result[0], neighborhood_name=result[1]
                    ),
                    location=pantry.location,
                    hours_start=pantry.hours_start,
                    hours_end=pantry.hours_end,
                    date_created=pantry.date_created,
                )
        except Exception:
            return {"message": "Could not update that pantry."}

    def get_all(self) -> Union[Error, List[PantryOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            pantries.id,
                            pantries.active,
                            pantries.pantry_name,
                            pantries.pantry_picture,
                            neighborhoods.id,
                            neighborhoods.neighborhood_name,
                            pantries.location,
                            pantries.hours_start,
                            pantries.hours_end,
                            pantries.date_created
                        FROM pantries
                        INNER JOIN neighborhoods ON pantries.neighborhood = neighborhoods.id
                        ORDER BY pantries.id
                        """
                    )
                    return [
                        PantryOut(
                            id=record[0],
                            active=record[1],
                            pantry_name=record[2],
                            pantry_picture=record[3],
                            neighborhood=Neighborhood(
                                id=record[4], neighborhood_name=record[5]
                            ),
                            location=record[6],
                            hours_start=record[7],
                            hours_end=record[8],
                            date_created=record[9],
                        )
                        for record in db
                    ]
        except Exception:
            return {"message": "Could not get all pantries"}

    def create(self, pantry: PantryIn) -> PantryOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO pantries
                        (active, pantry_name, pantry_picture, neighborhood, location, hours_start, hours_end, date_created)
                    VALUES
                        (%s, %s, %s, %s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        pantry.active,
                        pantry.pantry_name,
                        pantry.pantry_picture,
                        pantry.neighborhood,
                        pantry.location,
                        pantry.hours_start,
                        pantry.hours_end,
                        pantry.date_created,
                    ],
                )
                id = db.fetchone()[0]
                if id is None:
                    return None

                with conn.cursor() as db_neighborhood:
                    db_neighborhood.execute(
                        """
                    SELECT
                        id,
                        neighborhood_name
                        FROM neighborhoods
                        WHERE id = %s;
                    """,
                        [pantry.neighborhood],
                    )

                    result = db_neighborhood.fetchone()

                return PantryOut(
                    id=id,
                    active=pantry.active,
                    pantry_name=pantry.pantry_name,
                    pantry_picture=pantry.pantry_picture,
                    neighborhood=Neighborhood(
                        id=result[0], neighborhood_name=result[1]
                    ),
                    location=pantry.location,
                    hours_start=pantry.hours_start,
                    hours_end=pantry.hours_end,
                    date_created=pantry.date_created,
                )

    def pantry_in_to_out(self, id: int, pantry: PantryIn):
        old_data = pantry.dict()
        return PantryOut(id=id, **old_data)

    def record_to_pantry_out(self, record):
        return PantryOut(
            id=record[0],
            active=record[1],
            pantry_name=record[2],
            pantry_picture=record[3],
            neighborhood=Neighborhood(
                id=record[4], neighborhood_name=record[5]
            ),
            location=record[6],
            hours_start=record[7],
            hours_end=record[8],
            date_created=record[9],
        )
