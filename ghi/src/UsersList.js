import { Fragment } from "react";
import React, { useEffect, useState } from "react";
import useAToken from "./UIAuth.tsx";
import ErrorNotification from "./ErrorNotification.js";
import { useNavigate } from "react-router-dom";

export default function UsersList() {
  const [users, setUsers] = useState([]);
  const { fetchWithCookie, token, logout } = useAToken();
  // const [showModal, setShowModal] = useState(false);
  const [title] = useState("List of Volunteers");
  const navigate = useNavigate();

  async function getData() {
    let url = `${process.env.REACT_APP_API_HOST}/users/`;
    let response = await fetchWithCookie(url);
    let data = await response[0];
    let status = await response[1];

    if (status === true) {
      setUsers(data);
    } else {
      ErrorNotification(response);
    }
  }

  const handleDelete = async (userID) => {
    logout();
    const url = `${process.env.REACT_APP_API_HOST}/users/${userID}`;
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      navigate("/");
    } else {
      ErrorNotification(response);
    }
  };

  useEffect(() => {
    document.title = title;
    getData();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <section className="container px-4 mx-auto">
        <div className="flex items-center gap-x-3">
          <h1 className="mt-8 mb-8 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
            <span className="text-transparent bg-clip-text bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500">
              Volunteers
            </span>
          </h1>

          <span className="px-3 py-1 text-xs text-orange-600 bg-orange-100 rounded-full dark:bg-gray-800 dark:text-blue-400">
            {users.length}
          </span>
        </div>
        <div className="flex flex-col mt-6">
          <div className="mx-4 my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
              <div className="overflow-hidden border border-gray-200 dark:border-gray-700 md:rounded-lg">
                <table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
                  <thead className="bg-gray-50 dark:bg-gray-800">
                    <tr key="tablehead">
                      <th
                        scope="col"
                        className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                      >
                        <div className="flex items-center gap-x-3">
                          <input
                            type="checkbox"
                            className="text-blue-500 border-gray-300 rounded dark:bg-gray-900 dark:ring-offset-gray-900 dark:border-gray-700"
                          ></input>
                          <span>Name</span>
                        </div>
                      </th>
                      <th
                        scope="col"
                        className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                      >
                        <button className="flex items-center gap-x-2">
                          <span>Role</span>

                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth="2"
                            stroke="currentColor"
                            className="w-4 h-4"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9 5.25h.008v.008H12v-.008z"
                            />
                          </svg>
                        </button>
                      </th>

                      <th
                        scope="col"
                        className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                      >
                        Email address
                      </th>

                      <th scope="col" className="relative py-3.5 px-4">
                        <span className="sr-only">Edit</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900">
                    {users.map((user) => {
                      return (
                        <Fragment key={"data" + user.user_id}>
                          <tr>
                            <td
                              className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap"
                              key={"name" + user.user_id}
                            >
                              <div className="inline-flex items-center gap-x-3">
                                <input
                                  type="checkbox"
                                  className="text-blue-500 border-gray-300 rounded dark:bg-gray-900 dark:ring-offset-gray-900 dark:border-gray-700"
                                ></input>
                                <div className="flex items-center gap-x-2">
                                  <div>
                                    <h2 className="font-medium text-gray-800 dark:text-white ">
                                      {user.first_name}
                                    </h2>
                                    <p className="text-sm font-normal text-gray-600 dark:text-gray-400">
                                      {user.pronouns}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td
                              className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap"
                              key={"type" + user.user_id}
                            >
                              {user.type}
                            </td>
                            <td
                              className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap"
                              key={"email" + user.user_id}
                            >
                              {user.email}
                            </td>
                            <td
                              className="px-4 py-4 text-sm whitespace-nowrap"
                              key={"delete" + user.user_id}
                            >
                              <div className="flex items-center gap-x-6">
                                <button
                                  onClick={() => handleDelete(user.user_id)}
                                  className="text-gray-500 transition-colors duration-200 dark:hover:text-red-500 dark:text-gray-300 hover:text-red-500 focus:outline-none"
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth="1.5"
                                    stroke="currentColor"
                                    className="w-5 h-5"
                                  >
                                    <path
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                      d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                    />
                                  </svg>
                                </button>
                              </div>
                            </td>
                          </tr>
                        </Fragment>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
