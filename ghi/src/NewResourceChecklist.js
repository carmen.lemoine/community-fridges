import { useEffect } from "react";
import { Link } from "react-router-dom";

const NewResourceChecklist = () => {
  const title = "Checklist";

  useEffect(() => {
    document.title = title;
  }, []);

  return (
    <>
      <div className="max-w-md mx-auto mb-8 bg-white rounded-xl overflow-hidden md:max-w-2xl">
        <h1 className="prose prose-h1 mt-8 mb-8 text-3xl font-extrabold md:text-3xl lg:text-3xl">
          Checklist for setting up a new Fridge or Pantry
        </h1>
        <h2 className="prose prose-h2 mt-8 mb-8 text-2xl font-extrabold md:text-xl lg:text-2xl">
          BUILD A TEAM
        </h2>
        <p className="prose prose-lead prose-slate prose-xl">
          Connect with your local community organizers and/or a mutual aid
          network in your area. Identify other organizers and networks of people
          who can support your community fridge or pantry initiative. Ideally
          your team should consist of{" "}
          <span className="font-extrabold">
            several members who are born and raised in the neighborhood
          </span>{" "}
          you wish to set up a fridge or pantry in.
        </p>
        <ul className="prose prose-ul prose-slate prose-xl list-disc list-outside mt-4">
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Establish a point person who can be responsible for delegating tasks
            to the rest of the team and being the main point of contact for that
            team, along with an{" "}
            <span className="font-extrabold">alternate point person</span> if
            they become unavailable
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Add other{" "}
            <span className="font-extrabold">dedicated team members</span> who
            are committed to checking up on the fridge or pantry on a regular
            basis
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Check in with existing local mutual aid organizations who have
            already established connections in the neighborhood and offer your
            help to them
          </li>
        </ul>
        <h2 className="prose prose-h2 mt-8 mb-8 text-2xl font-extrabold md:text-xl lg:text-2xl">
          MODES OF COMMUNICATION
        </h2>
        <p className="prose prose-lead prose-slate prose-xl">
          Decide how your group will communicate and begin strategizing with
          your group on how to spread the word, how to easily get others
          involved. Additionally, if there is an issue with the fridge or
          pantry, whether that is low stock or power is out, how will you report
          those issues?
        </p>
        <ul className="prose prose-ul prose-slate prose-xl list-disc list-outside mt-4">
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Your group has the option of setting up a free{" "}
            <span className="font-extrabold">Slack</span> workspace (a messaging
            service) where you can create a channel/chat specifically for your
            neighborhood's fridge or pantry.
          </li>
        </ul>
        <h2 className="prose prose-h2 mt-8 mb-8 text-2xl font-extrabold md:text-xl lg:text-2xl">
          DISCUSS THE LOGISTICS
        </h2>
        <p className="prose prose-lead prose-slate prose-xl">
          Where will the food come from? Who will pick it up? Who has a bike or
          a vehicle? How will you obtain cleaning supplies to help keep the
          fridge or pantry clean? There are many tasks required to collectively
          maintain the community fridge or pantry, from collecting food,
          reaching out to businesses and community mutual aid networks, to
          cleaning, restocking, sharing information, making art and more. Make a
          detailed plan.
        </p>
        <h2 className="prose prose-h2 mt-8 mb-8 text-2xl font-extrabold md:text-xl lg:text-2xl">
          REACH OUT TO FOOD DONORS
        </h2>
        <p className="prose prose-lead prose-slate prose-xl">
          Much of the food shared has been through food rescue, including
          dumpster diving and non-charitable food donations (i.e. food that a
          grocery store cannot or is not planning to sell such as produce with
          cosmetic blemishes or food near it's best-buy date). In order for a
          fridge or pantry to be continually active, look for businesses in your
          area who can commit to making recurring donations, like on a weekly
          basis for example. You are not obligated to “shout out” any businesses
          who make donations. Donations should be made with the goal of helping
          out the community and diverting food waste, not for the sole purpose
          of getting good PR. Some good businesses to reach out to are:
        </p>
        <ul className="prose prose-ul prose-slate prose-xl list-disc list-outside mt-4">
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Local grocery stores
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Bakeries
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Cafes
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Catering companies
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Restaurants
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Coordinate{" "}
            <span className="font-extrabold">regular pick-up times</span> with
            any businesses who agree to donate, connect them with a community
            volunteer
          </li>
        </ul>
        <h2 className="prose prose-h2 mt-8 mb-8 text-2xl font-extrabold md:text-xl lg:text-2xl">
          ESTABLISH A SCHEDULE
        </h2>
        <p className="prose prose-lead prose-slate prose-xl">
          Assign tasks to people in your group depending on your skills and
          resources.
        </p>
        <ul className="prose prose-ul prose-slate prose-xl list-disc list-outside mt-4">
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Daily check-ins to report levels of stock.
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Cleaning schedule.
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Regular drop-offs of donated food from nearby businesses.
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Maintenance or service schedule.
          </li>
        </ul>
        <h2 className="prose prose-h2 mt-8 mb-8 text-2xl font-extrabold md:text-xl lg:text-2xl">
          HOST LOCATION
        </h2>
        <p className="prose prose-lead prose-slate prose-xl">
          Decide on a location for the fridge or pantry. Make considerations
          that account for electricity, weather and accessibility. Ideally, we
          would like you to work with a long standing neighborhood business that
          is not a gentrifying business, a place where all people in the
          community, including unhoused residents, will feel comfortable
          gathering and accessing. We strongly suggest that you do not set up
          fridges or pantries in locations that collaborate with law enforcement
          because that creates an unsafe environment for our community members.
          Ensure that the business or residence interested in hosting has enough
          space where the fridge or pantry won't block the sidewalk or cause
          unnecessary extra trouble for those with disabilities navigating the
          sidewalk. Some additional questions to consider are:
        </p>
        <ul className="prose prose-ul prose-slate prose-xl list-disc list-outside mt-4">
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            <span className="font-extrabold">
              Is the host location close to or within walking distance to
              unhoused encampments?
            </span>{" "}
            Is it in a high traffic area easy to access?
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Is the host location already close to{" "}
            <span className="font-extrabold">
              any other community fridges or pantries?
            </span>
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            <span className="font-extrabold">
              Is the host willing to donate the cost of the electricity for the
              fridge and lighting?
            </span>{" "}
            It is expected that the host will be able to donate the electricity
            cost for fridge use because you should try to operate on a donations
            and volunteer basis as much as possible.
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            If the host is a business,{" "}
            <span className="font-extrabold">
              who are the business owners? How are they involved in the
              community and what is their role in the community?
            </span>{" "}
            Although a community fridge or pantry are not meant to be used as a
            means to increase traffic to a store and gain good PR, we assume
            that it can lead to a slight increase in business to that store.
            Therefore, we suggest trying to{" "}
            <span className="font-extrabold">
              prioritize hosts that are BIPOC-owned stores and are not
              gentrifying the neighborhood they reside in.
            </span>{" "}
            This also helps ensure that all community members feel welcome to
            access a fridge or pantry, for it seems like gentrifying businesses,
            although usually acting in good faith and mean no harm, can
            unknowingly create an environment that feels unwelcome to long
            standing community residents.
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            We strongly suggest that you do not support or collaborate with any
            organizations that support or work with law enforcement or any
            government bodies.{" "}
            <span className="font-extrabold">
              Does this host share your network's values of keeping all
              community members safe?
            </span>
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            <span className="font-extrabold">
              Are people at this location able to give permission or consent to
              set up the fridge or pantry?
            </span>{" "}
            Some business owners or residents are not owners of the property you
            intend to set up a fridge or pantry on, so it is best to get at
            least verbal permission from an owner, landlord, or property manager
            directly. If there are worries about liability, let them know that
            the fridge or pantry will be operated independently from the
            property/business. More information on liability insurance can be
            found on the{" "}
            <a
              target="_blank"
              href="https://freedge.org/freedge-yourself/"
              rel="noreferrer"
            >
              Freedge website.
            </a>
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            <span className="font-extrabold">
              Ensure that you're chosen host reads and agrees to these{" "}
              <Link to="/" alt="Host Guidelines">
                Host Guidelines.
              </Link>
            </span>
          </li>
        </ul>
        <h2 className="prose prose-h2 mt-8 mb-8 text-2xl font-extrabold md:text-xl lg:text-2xl">
          ACQUIRE A FRIDGE OR PANTRY
        </h2>
        <p className="prose prose-lead prose-slate prose-xl">
          It is easy to acquire a fridge or pantry for little to no cost, we
          suggest searching Craigslist, neighborhood message boards such as
          Nextdoor, or even just finding fridges or pantries left out on the
          street that are free to take. We have also suggest putting out a call
          for fridge or pantry donations on social media or simply spreading the
          word to neighborhood residents.{" "}
          <span className="font-extrabold">
            You might also ask for help building a new pantry from local
            contractors or handypersons.
          </span>
        </p>
        <ul className="prose prose-ul prose-slate prose-xl list-disc list-outside mt-4">
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Figure out how to{" "}
            <span className="font-extrabold">
              transport the fridge or pantry.
            </span>{" "}
            Ask someone with a large vehicle such as a truck or van to help. Do
            not expect that the donor/seller of the fridge or pantry you are
            going to acquire can provide their own transportation.{" "}
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Acquire any necessary accessories such as extension cords for your
            fridge's host. Try to provide these yourselves, do not expect that
            the host has{" "}
            <span className="font-extrabold">extra extension cords</span> just
            lying around.
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Make sure it can be plugged into an outlet without overloading the
            circuit.
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            We recommend doing any{" "}
            <span className="font-extrabold">painting or decorating</span> of
            the fridge or pantry before publicly launching it receiving
            donations. Why? (A) So that it can be finished and out of the way
            before people start using it, it can be difficult with some paints
            taking a long time to dry with community members also trying to
            access the fridge or pantry for food. (B) Your fridge or pantry
            could get swiped if not properly marked! An unmarked fridge or
            pantry on a sidewalk can easily be mistaken for someone giving it
            out free for the taking.
          </li>
        </ul>
        <h2 className="prose prose-h2 mt-8 mb-8 text-2xl font-extrabold md:text-xl lg:text-2xl">
          SPREAD THE WORD
        </h2>
        <p className="prose prose-lead prose-slate prose-xl">
          Let folks in your community you have an active free fridge up and
          running! Make it clear you can “take what you need and leave what you
          don't.”
        </p>
        <ul className="prose prose-ul prose-slate prose-xl list-disc list-outside mt-4">
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Print fliers and posters to post around the neighborhood to let
            those who don't have internet access know where the fridge is
            located. Try to include visual aids such as a basic map for those
            who can't read and any necessary language translations that are
            primarily spoken by your neighborhood's residents in addition to
            English.
          </li>
        </ul>
        <h2 className="prose prose-h2 mt-8 mb-8 text-2xl font-extrabold md:text-xl lg:text-2xl">
          MAINTAIN THE MOMENTUM
        </h2>
        <p className="prose prose-lead prose-slate prose-xl">
          Work together to keep the fridge or pantry clean and stocked. This
          requires daily coordination, follow-ups, food rescue, pickup and
          delivery to the fridge or pantry.
        </p>
        <ul className="prose prose-ul prose-slate prose-xl list-disc list-outside mt-4">
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Find the best way to stay in contact with your team and other
            community members, whether through Slack, Instagram, email, text
            message, or another service.{" "}
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Stick to your schedule of cleaning and stocking. This is an ongoing
            project that requires sustained attention, do not assume that
            because you have dropped off a free fridge or pantry it will
            magically stock or clean itself.
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Constantly keep in touch and communicate with residents in the
            neighborhood who use your fridge or pantry!
          </li>
          <li>
            <input
              type="checkbox"
              className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500 mr-4"
            />
            Accept criticism without pushback. Make adjustments to your fridge
            or pantry set up as necessary. Again, listen to your community and
            respond to community needs.
          </li>
        </ul>
      </div>
    </>
  );
};

export default NewResourceChecklist;
