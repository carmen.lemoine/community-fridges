async function getNeighborhoodName(id) {
    const url = `${process.env.REACT_APP_API_HOST}/neighborhoods/${id}`;
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        return data;
    } else {
        console.error(response)
    }
}

export default getNeighborhoodName;
