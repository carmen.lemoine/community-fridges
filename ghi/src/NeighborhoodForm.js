import React, { useEffect, useState } from "react";
import useAToken from "./UIAuth.tsx";
import { useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import house from "./house.json";

const CreateNeighborhood = () => {
  const [postalCode, setPostalCode] = useState("");
  const [neighborhoodName, setNeighborhoodName] = useState("");
  const { token } = useAToken();
  const navigate = useNavigate();
  const title = "Add a New Neighborhood";

  const handleNeighborhoodCreation = async (e) => {
    e.preventDefault();
    const neighborhoodData = {
      neighborhood_name: neighborhoodName,
    };
    const neighborhoodURL = `${process.env.REACT_APP_API_HOST}/neighborhoods`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(neighborhoodData),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(neighborhoodURL, fetchConfig);
    if (response.ok) {
      const neighborhoodResponse = await response.json();
      const postalCodeData = {
        postal_code: postalCode,
        neighborhood_id: neighborhoodResponse.id,
      };
      const postalCodeURL = `${process.env.REACT_APP_API_HOST}/postal_codes`;
      const postalCodeResponse = await fetch(postalCodeURL, {
        method: "POST",
        headers: fetchConfig.headers,
        body: JSON.stringify(postalCodeData),
      });
      if (postalCodeResponse.ok) {
        e.target.reset();
        navigate("/neighborhoods");
      } else {
        console.error(postalCodeResponse);
      }
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    document.title = title;
  });

  if (!token) {
    navigate("/login");
  }
  return (
    <div className="relative min-h-screen flex items-center justify-center">
      <section className="max-w-4xl p-6 mx-auto bg-white rounded-md shadow-md dark:bg-gray-800 mt-[-20rem]">
        <h2 className="text-xl font-semibold capitalize gradient-text">
          Create A Neighborhood
        </h2>
        <form onSubmit={handleNeighborhoodCreation}>
          <div className="grid grid-cols-1 gap-6 mt-4 sm:grid-cols-2">
            <div>
              <label
                className="text-gray-700 dark:text-gray-200"
                htmlFor="neighborhoodName"
              >
                Neighborhood Name:
              </label>
              <input
                id="neighborhoodName"
                type="text"
                className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
                value={neighborhoodName}
                onChange={(e) => setNeighborhoodName(e.target.value)}
              />
            </div>
            <div>
              <label
                className="text-gray-700 dark:text-gray-200"
                htmlFor="postalCode"
              >
                Postal Code:
              </label>
              <input
                id="postalCode"
                type="text"
                className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
                value={postalCode}
                onChange={(e) => setPostalCode(e.target.value)}
              />
            </div>
          </div>
          <div className="flex justify-end mt-6">
            <button
              type="submit"
              className="px-8 py-2.5 leading-5 text-white transition-colors duration-300 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600 gradient-button"
            >
              Submit
            </button>
          </div>
        </form>
      </section>
      <div className="absolute bottom-0 inset-x-0 flex justify-center mb-4">
        <Lottie
          animationData={house}
          style={{
            height: "30%",
            zIndex: -1,
            overflow: "hidden",
            position: "static",
          }}
          loop={false}
        />
      </div>
    </div>
  );
};

export default CreateNeighborhood;
