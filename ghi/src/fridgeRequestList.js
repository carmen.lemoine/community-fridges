import { useState, useEffect } from "react";
import useAToken from "./UIAuth.tsx";
import { fetchData } from "./utils/fetchdata";

export default function FridgeRequests() {
  const [requests, setRequests] = useState([]);
  const { fetchWithCookie } = useAToken();
  const [fridgeList, setFridgeList] = useState([]);
  const [items, setItemsList] = useState([]);
  const { token } = useAToken();
  const [isLoading, setIsLoading] = useState(true);
  const title = "Fridge Requests";

  const fetchProtectedData = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/fridge-requests/`;
    const response = await fetchWithCookie(url);
    const status = await response[1];
    if (status) {
      const data = await response[0];
      setRequests(data);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchProtectedData();
    fetchData("/fridges", setFridgeList);
    fetchData("/items/", setItemsList);
    document.title = title;
    // eslint-disable-next-line
  }, []);

  if (token) {
    if (isLoading) {
      return <div>Loading Item Requests</div>;
    } else {
      return (
        <>
          <section className="container px-4">
            <div className="flex items-center gap-4">
              <h1 className="mt-6 mb-6 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
                <span className="text-transparent bg-clip-text bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500">
                  Fridge Requests
                </span>
              </h1>
              <span className="px-3 py-1 text-xs text-blue-600 bg-blue-100 rounded-full dark:bg-gray-800 dark:text-blue-400">
                {requests.length} Requests Received
              </span>
            </div>
            <div className="flex flex-col mt-6">
              <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                  <div className="overflow-hidden border border-gray-200 dark:border-gray-700 md:rounded-lg">
                    <table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
                      <thead className="bg-gray-50 dark:bg-gray-800">
                        <tr>
                          <th
                            scope="col"
                            className="py-3.5 px-4 text-  sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            Fridge
                          </th>

                          <th
                            scope="col"
                            className="px-12 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            Date
                          </th>

                          <th
                            scope="col"
                            className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            Additional
                          </th>

                          <th
                            scope="col"
                            className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            Visitor
                          </th>

                          <th
                            scope="col"
                            className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            Items Requested
                          </th>
                        </tr>
                      </thead>
                      <tbody className="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900">
                        {requests.map((request) => {
                          let fridgeIndex = request.id - 1;
                          let itemsListed = [];
                          for (let item of request.items) {
                            let itemIndex = item - 1;
                            let itemName = items[itemIndex].name;
                            itemsListed.push(itemName);
                          }
                          return (
                            <tr key={request.id}>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="inline-flex items-center gap-x-3">
                                  {fridgeList[fridgeIndex].fridge_name}
                                </div>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="inline-flex items-center gap-x-3">
                                  {request.date_created}
                                </div>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="inline-flex items-center gap-x-3">
                                  {request.additional}
                                </div>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="inline-flex items-center gap-x-3">
                                  {request.visitor}
                                </div>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="inline-flex items-center gap-x-3">
                                  {itemsListed.map((item) => {
                                    return (
                                      <span
                                        key={`item` + item}
                                        className="inline-flex items-center justify-center rounded-full bg-rose-600 px-2.5 py-0.5 text-white"
                                      >
                                        {item}
                                      </span>
                                    );
                                  })}
                                </div>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
      );
    }
  } else {
    return (
      <div
        role="alert"
        className="rounded border-s-4 border-red-500 bg-red-50 p-4"
      >
        <div className="flex items-center gap-2 text-red-800">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="h-5 w-5"
          >
            <path
              fillRule="evenodd"
              d="M9.401 3.003c1.155-2 4.043-2 5.197 0l7.355 12.748c1.154 2-.29 4.5-2.599 4.5H4.645c-2.309 0-3.752-2.5-2.598-4.5L9.4 3.003zM12 8.25a.75.75 0 01.75.75v3.75a.75.75 0 01-1.5 0V9a.75.75 0 01.75-.75zm0 8.25a.75.75 0 100-1.5.75.75 0 000 1.5z"
              clipRule="evenodd"
            />
          </svg>
          <strong className="block font-medium">
            {" "}
            Sorry ! Access not authorized.{" "}
          </strong>
        </div>

        <p className="mt-2 text-sm text-red-700">
          You need to Login to see this page T_T
        </p>
      </div>
    );
  }
}
