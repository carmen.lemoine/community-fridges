import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import { fetchData } from "./utils/fetchdata.js";
import NotFound from "./NotFound.js";
import useAToken from "./UIAuth.tsx";
import Lottie from "lottie-react";
import needLoginAnimation from "./needLoginAnimation.json";

const PantryDetail = ({ convertTimeFormat, convertDateTimeFormat }) => {
  const { id } = useParams();
  const [pantry, setPantry] = useState(null);
  const [pantryCheckIns, setPantryCheckIns] = useState([]);
  const [itemEntries, setItemEntries] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { token } = useAToken();
  const title = "Pantry Detail";

  async function getAPantry(id) {
    const url = `${process.env.REACT_APP_API_HOST}/pantries/${id}`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setPantry(data);
    } else {
      setPantry([]);
      console.error(response);
    }
    setIsLoading(false);
  }

  async function getPantryCheckins(id) {
    const url = `${process.env.REACT_APP_API_HOST}/api/pantrycheckins/${id}`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setPantryCheckIns(data);
    } else {
      setPantryCheckIns(null);
      console.error(response);
    }
  }

  const renderStars = (clean_status) => {
    const totalStars = 3;
    const filledStars = Math.min(clean_status, totalStars);

    const stars = [];
    for (let i = 0; i < totalStars; i++) {
      stars.push(
        <svg
          key={i}
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          fill={i < filledStars ? "currentColor" : "none"}
          className="w-6 h-6 text-yellow-500"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M11.48 3.499a.562.562 0 011.04 0l2.125 5.111a.563.563 0 00.475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 00-.182.557l1.285 5.385a.562.562 0 01-.84.61l-4.725-2.885a.563.563 0 00-.586 0L6.982 20.54a.562.562 0 01-.84-.61l1.285-5.386a.562.562 0 00-.182-.557l-4.204-3.602a.563.563 0 01.321-.988l5.518-.442a.563.563 0 00.475-.345L11.48 3.5z"
          />
        </svg>
      );
    }

    return (
      <p className="inline-flex items-center flex-wrap gap-1 p-4">
        Cleanliness {stars}{" "}
      </p>
    );
  };

  const getItemDetails = (checkinItems) => {
    const names = [];
    if (pantryCheckIns.length > 0) {
      for (let good of checkinItems) {
        const result = itemEntries.find((item) => item.id === good);
        names.push(result.name);
      }
    }
    return names;
  };

  useEffect(() => {
    const fetchPantryData = async () => {
      await Promise.all([getAPantry(id), getPantryCheckins(id)])
        .then(() => setIsLoading(false))
        .catch((error) => {
          console.error(error);
          setIsLoading(false);
        });
    };
    fetchPantryData();
    fetchData("/items/", setItemEntries);
  }, [id]);

  useEffect(() => {
    document.title = title;
  }, []);

  const last_checkin = pantryCheckIns[0];

  if (isLoading) {
    return <div>Loading...</div>;
  } else if (!pantry) {
    return <NotFound />;
  }

  if (!pantry.active && !token) {
    return (
      <section className="bg-white dark:bg-gray-900 ">
        <div className="container min-h-screen px-6 py-12 mx-auto lg:flex lg:items-center lg:gap-12">
          <div className="wf-ull lg:w-1/2">
            <p className="text-sm font-medium text-blue-500 dark:text-blue-400">
              Authentication error
            </p>
            <h1 className="mt-3 text-2xl font-semibold text-gray-800 dark:text-white md:text-3xl">
              Inactive Pantry
            </h1>
            <p className="mt-4 text-gray-500 dark:text-gray-400">
              Sorry, This pantry is currently inactive. You need to login to
              view the details.
            </p>

            <div className="flex items-center mt-6 gap-x-3">
              <button className="w-1/2 px-5 py-2 text-sm tracking-wide text-white transition-colors duration-200 bg-blue-500 rounded-lg shrink-0 sm:w-auto hover:bg-blue-600 dark:hover:bg-blue-500 dark:bg-blue-600">
                <Link to="/login">Login</Link>
              </button>
            </div>
          </div>

          <div className="relative w-full mt-8 lg:w-1/2 lg:mt-0">
            <Lottie animationData={needLoginAnimation} loop={true} />
          </div>
        </div>
      </section>
    );
  }

  return (
    <section className="relative flex h-screen w-screen flex-col justify-center overflow-hidden bg-white py-6 sm:py-12">
      <div className="lg:grid lg:min-h-screen lg:grid-cols-12">
        <section className="relative flex h-32 items-start bg-gray-900 lg:col-span-5 lg:h-full xl:col-span-6">
          <img
            alt="Stocked Pantry"
            src={pantry.pantry_picture}
            className="absolute inset-0 h-full w-full object-cover opacity-60"
          />
          <div className="hidden lg:relative lg:block lg:p-12">
            <h2 className="mt-6 text-2xl font-bold text-white sm:text-3xl md:text-4xl">
              {pantry.pantry_name}
            </h2>
            <p className="mt-4 pb-4 leading-relaxed text-white/90">
              <span className="flex items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
                  />
                </svg>{" "}
                <span className="pl-2">{pantry.location}</span>
              </span>
            </p>
            <a
              href={`https://www.google.com/maps/dir/?api=1&destination=${encodeURIComponent(
                pantry.location
              )}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <button className="w-1/2 px-5 py-2 text-sm tracking-wide text-white transition-colors duration-200 bg-blue-500 rounded-lg shrink-0 sm:w-auto hover:bg-blue-600 dark:hover:bg-blue-500 dark:bg-blue-600">
                Get Directions
              </button>
            </a>
          </div>
        </section>
        <main className="flex items-start justify-left px-8 py-8 sm:px-12 lg:col-span-7 lg:px-16 lg:py-12 xl:col-span-6">
          <div className="max-w-xl lg:max-w-3xl">
            <div className="relative block lg:hidden">
              <span className="sr-only">{pantry.pantry_name}</span>

              <h1 className="mt-2 text-2xl font-bold text-gray-900 sm:text-3xl md:text-4xl">
                {pantry.pantry_name}
              </h1>

              <p className="mt-4 leading-relaxed text-gray-500">
                <span className="flex items-center">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
                    />
                  </svg>{" "}
                  {pantry.location}
                </span>
              </p>
              <a
                href={`https://www.google.com/maps/dir/?api=1&destination=${encodeURIComponent(
                  pantry.location
                )}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                <button className="inline-block shrink-0 rounded-md border border-blue-600 bg-blue-600 px-12 py-3 text-sm font-medium text-white transition hover:bg-transparent hover:text-blue-600 focus:outline-none focus:ring active:text-blue-500">
                  Get Directions
                </button>
              </a>
            </div>

            <section className="pb-4">
              <dt className="sr-only">Pantry Hours</dt>
              <dd className="prose prose-slate">
                Pantry Hours:{" "}
                <time dateTime={`${pantry.hours_start}`}>
                  {convertTimeFormat(pantry.hours_start)}
                </time>
                {" to "}{" "}
                <time dateTime={`${pantry.hours_end}`}>
                  {convertTimeFormat(pantry.hours_end)}
                </time>
              </dd>
            </section>
            <section className="pb-4">
              <dt className="sr-only">Status</dt>
              <dd className="prose prose-slate">
                Status: {pantry.active ? "Active" : "Inactive"}
              </dd>
            </section>

            <section className="pb-4">
              <h2 className="mb-6"> Latest Check-in</h2>
              <div className="w-full text-gray-900 bg-white border border-gray-200 rounded-lg dark:bg-gray-700 dark:border-gray-600 dark:text-white">
                {pantryCheckIns.length !== 0 ? (
                  <React.Fragment>
                    {" "}
                    <button
                      type="button"
                      className="relative inline-flex items-center w-full px-4 py-2 text-sm font-medium border-b border-gray-200 rounded-t-lg hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z"
                        />
                      </svg>

                      <p className="p-4">
                        <time dateTime={`${last_checkin.date_time}`}>
                          {convertDateTimeFormat(last_checkin.date_time)}
                        </time>
                      </p>
                    </button>
                    <button
                      type="button"
                      className="relative inline-flex items-center w-full px-4 py-2 text-sm font-medium border-b border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M9.813 15.904L9 18.75l-.813-2.846a4.5 4.5 0 00-3.09-3.09L2.25 12l2.846-.813a4.5 4.5 0 003.09-3.09L9 5.25l.813 2.846a4.5 4.5 0 003.09 3.09L15.75 12l-2.846.813a4.5 4.5 0 00-3.09 3.09zM18.259 8.715L18 9.75l-.259-1.035a3.375 3.375 0 00-2.455-2.456L14.25 6l1.036-.259a3.375 3.375 0 002.455-2.456L18 2.25l.259 1.035a3.375 3.375 0 002.456 2.456L21.75 6l-1.035.259a3.375 3.375 0 00-2.456 2.456zM16.894 20.567L16.5 21.75l-.394-1.183a2.25 2.25 0 00-1.423-1.423L13.5 18.75l1.183-.394a2.25 2.25 0 001.423-1.423l.394-1.183.394 1.183a2.25 2.25 0 001.423 1.423l1.183.394-1.183.394a2.25 2.25 0 00-1.423 1.423z"
                        />
                      </svg>
                      Cleanliness: {renderStars(last_checkin.clean_status)}
                    </button>
                    <button
                      type="button"
                      className="relative inline-flex items-center w-full px-4 py-2 text-sm font-medium border-b border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M3.75 12h16.5m-16.5 3.75h16.5M3.75 19.5h16.5M5.625 4.5h12.75a1.875 1.875 0 010 3.75H5.625a1.875 1.875 0 010-3.75z"
                        />
                      </svg>

                      <p className="inline-flex items-center gap-x-1 p-4">
                        In Stock:
                        {getItemDetails(last_checkin.stocked_items).map(
                          (cat, index) => {
                            return (
                              <span
                                key={`item` + index}
                                className="inline-flex items-center justify-center rounded-full bg-rose-600 px-2.5 py-0.5 text-white"
                              >
                                {cat}
                              </span>
                            );
                          }
                        )}
                      </p>
                    </button>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <button
                      type="button"
                      className="relative inline-flex items-center w-full px-4 py-2 text-sm font-medium border-b border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 9v3.75m9-.75a9 9 0 11-18 0 9 9 0 0118 0zm-9 3.75h.008v.008H12v-.008z"
                        />
                      </svg>

                      <p className="p-4">
                        There haven't been any check-ins at this pantry yet.
                      </p>
                    </button>
                  </React.Fragment>
                )}
              </div>
            </section>
            <section>
              <Link to="/pantrycheckins/new">
                <button className="w-1/2 px-5 py-2 text-sm tracking-wide text-white transition-colors duration-200 bg-blue-500 rounded-lg shrink-0 sm:w-auto hover:bg-blue-600 dark:hover:bg-blue-500 dark:bg-blue-600">
                  Add a check-in
                </button>
              </Link>
            </section>
          </div>
        </main>
      </div>
    </section>
  );
};

export default PantryDetail;
